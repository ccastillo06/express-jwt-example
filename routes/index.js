const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');

const secret = 'UpgradeSecret';

const user = {
  id: 1,
  email: 'upgrade@hub.com',
  password: '1234abc',
};

router.get('/', (req, res) => res.sendStatus(200));

router.post('/login', (req, res) => {
  const { email, password } = req.body;

  if (email === user.email && password === user.password) {
    const payload = {
      id: user._id,
      email: user.email,
    };

    jwt.sign(payload, secret, { expiresIn: 36000 }, (err, token) => {
      if (err) {
        return res.status(500).json({ error: 'Error signing token', raw: err });
      }

      return res.status(200).json({
        success: true,
        token: token,
      });
    });
  } else {
    return res.status(401).json({ message: 'Incorrect email or password' });
  }
});

module.exports = router;
